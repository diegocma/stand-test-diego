
Sql gerado pelo Entity Framework

CREATE TABLE [dbo].[Cliente] (
    [Id]             INT             IDENTITY (1, 1) NOT NULL,
    [Razao_social]   NVARCHAR (MAX)  NOT NULL,
    [Cnpj]           NVARCHAR (MAX)  NOT NULL,
    [Data_fundacao]  DATETIME2 (7)   NOT NULL,
    [Capital]        DECIMAL (18, 2) NOT NULL,
    [Quarentena]     BIT             NOT NULL,
    [Status_cliente] BIT             NOT NULL,
    [Classificacao]  NVARCHAR (1)    NOT NULL,
    CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED ([Id] ASC)
);