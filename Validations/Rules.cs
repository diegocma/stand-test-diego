﻿using StandTestDiego.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StandTestDiego.Validations
{
    public class Rules
    {
        public Cliente Check (Cliente cliente)
        {
            DateTime d1 = DateTime.Today; //Inicio validação campo quarentena.
            d1 = d1.AddYears(-1);

            DateTime d2 = cliente.Data_fundacao;

            int x = DateTime.Compare(d1, d2);

            if (x <= 0)
            {
                cliente.Quarentena = true; //Marca em quarentena quando a data informada é menor ou igual do que há 1 ano.
            }

            if (cliente.Capital <= 10000.00M) // Inicio validação de classificação
            {
                cliente.Classificacao = 'C';
            }
            else if (cliente.Capital > 10000.00M && cliente.Capital <= 1000000.00M)
            {
                cliente.Classificacao = 'B';
            }
            else
            {
                cliente.Classificacao = 'A';
            }

            return cliente;
        }
    }
}
