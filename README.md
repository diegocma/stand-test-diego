----------------------------------
 Teste Stand Web App
 Nome: Diego Fernandes da Silva
 ----------------------------------
 
 Data: 14/11/2019

 Alterações:

 - Nova classe "Rules" no pacote "Validations" concentrando toda a regra de negócio do app.
 
 - Alterações do Controller Clientes para considerar a nova classe.
 
 - Exibição correta dos campos:
    ."Data de Fundação" no formato "dd/MM/yyyy" nas views Clientes/Index, Details e Delete;
	."Status" como Ativo ou Inativo substituindo o checkbox nas views Clientes/Index, Details e Delete;
    ."Quarentena" como Sim ou Não substituindo o radio nas views Details e Delete.
 
 - Remoção de classes desnecessárias.

 ---
 
 Data: 12/11/2019
 Notas Iniciais:
 
 * Ao inicializar o app o sistema irá carregar o cadastro de clientes com 3 empresas. Os dados podem ser alterados e deletados.
 
 * Caso dê algum erro com o BD e seja necessário criar as tabelas locais, executar os comandos abaixo em Tools -> NuGet Package Manager -> Package Manager Console (PMC).
 
 Install-Package Microsoft.EntityFrameworkCore.SqlServer
 Add-Migration InitialCreate
 Update-Database
 
 * Em novos cadastros ou edições serão feitas as validações da regra de negócio do projeto.
 
 * Optei por exibir todos os campos do DER na página de "Detalhes". Assim fica mais fácil conferir se todas as informações estão sendo registradas corretamente.
 
 * Os scripts do BD está inserido no arquivo "SCRIPTS.sql" na pasta raiz do projeto.
 
 
 Atenciosamente,
 Diego Fernandes da Silva