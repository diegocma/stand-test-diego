﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StandTestDiego.Models
{
    public class Cliente
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Razão Social")]
        public string Razao_social { get; set; }

        [Required]
        [Display(Name = "CNPJ")]
        public string Cnpj { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data de Fundação")]
        public DateTime Data_fundacao { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Capital { get; set; }

        [Required]
        public bool Quarentena { get; set; }

        [Required]
        [Display(Name = "Status")]
        public bool Status_cliente { get; set; }

        [Required]
        [Display(Name = "Classificação")]
        public char Classificacao { get; set; }

    }
}
