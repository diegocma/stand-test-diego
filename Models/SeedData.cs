﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StandTestDiego.Data;
using System;
using System.Linq;

namespace StandTestDiego.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new StandTestDiegoContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<StandTestDiegoContext>>()))
            {
                //Procura por clientes
                if (context.Cliente.Any())
                {
                    return; // DB já foi preenchido
                }

                context.Cliente.AddRange(
                    new Cliente
                    {
                        Razao_social = "Empresa A",
                        Cnpj = "00.111.222/3333-44",
                        Data_fundacao = DateTime.Parse("2005-01-10"),
                        Capital = 1000000.00M,
                        Status_cliente = true,
                        Quarentena = false,
                        Classificacao = 'B'
                    },

                    new Cliente
                    {
                        Razao_social = "Empresa B",
                        Cnpj = "55.676.777/8888-99",
                        Data_fundacao = DateTime.Parse("2019-09-23"),
                        Capital = 12345678.90M,
                        Status_cliente = true,
                        Quarentena = true,
                        Classificacao = 'A'
                    },

                    new Cliente
                    {
                        Razao_social = "Empresa C",
                        Cnpj = "99.000.111/2222-33",
                        Data_fundacao = DateTime.Parse("2018-05-07"),
                        Capital = 5000.00M,
                        Status_cliente = false,
                        Quarentena = false,
                        Classificacao = 'C'
                    }

                );
                context.SaveChanges();
            }
        }
    }
}
