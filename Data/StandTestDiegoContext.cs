﻿using Microsoft.EntityFrameworkCore;
using StandTestDiego.Models;

namespace StandTestDiego.Data
{
    public class StandTestDiegoContext : DbContext
    {
        public StandTestDiegoContext (DbContextOptions<StandTestDiegoContext> options)
            : base(options)
        {
        }

        public DbSet<Cliente> Cliente { get; set; }
    }
}
