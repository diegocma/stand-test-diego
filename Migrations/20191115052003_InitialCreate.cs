﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StandTestDiego.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Razao_social = table.Column<string>(nullable: false),
                    Cnpj = table.Column<string>(nullable: false),
                    Data_fundacao = table.Column<DateTime>(nullable: false),
                    Capital = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Quarentena = table.Column<bool>(nullable: false),
                    Status_cliente = table.Column<bool>(nullable: false),
                    Classificacao = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cliente");
        }
    }
}
